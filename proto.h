/*
  The idea of this file is to include prototypes for all external
  functions that rc uses, either by including the appropriate header
  file, or---for older systems---declaring the functions directly.
*/

/* rc23 requires C99 and a POSIX.1-2001, SUSv3 compliant system. */
#define _XOPEN_SOURCE 600

#define _FILE_OFFSET_BITS 64

#include <sys/types.h>
#include <signal.h>
#include <stdint.h>
#include <stdarg.h>

#include <stdlib.h>
#include <string.h>

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#if HAVE_SETPGRP

#if SETPGRP_VOID
/* Smells like POSIX: should all be ok. */
#else
/* Old BSD: fake it. */
#define setpgid(pid, pgrp) setpgrp(pid, pgrp)
#include <sys/ioctl.h>
#define tcgetpgrp(fd) ioctl((fd), TIOCGPGRP)
#define tcsetpgrp(fd, pgrp) ioctl((fd), TIOCSPGRP, &(pgrp))
#endif

#else /* HAVE_SETPGRP */

/* Nothing doing. */
#define setpgid()
#define tcgetpgrp()
#define tcsetpgrp()

#endif /*HAVE_SETPGRP */


/* fake errno.h for mips (which doesn't declare errno in errno.h!?!?) */
#ifdef host_mips
extern int errno;
#endif

#ifndef __dead
#if __GNUC__ || __clang__
#define __dead __attribute__((__noreturn__))
#else
#define __dead
#endif
#endif /* __dead */
